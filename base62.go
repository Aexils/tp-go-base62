package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	_ "github.com/kare/base62"
	"log"
	"net/http"
)

const (
	host     = "127.0.0.1"
	database = "base"
	user     = "root"
	password = ""
)

var connectionString = fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?allowNativePasswords=true", user, password, host, database)
var db, err = sql.Open("mysql", connectionString)

func connectDB() {

	checkError(err)

	err = db.Ping()
	checkError(err)
	fmt.Println("Successfully created connection to database.")

	_, err = db.Exec("DROP TABLE IF EXISTS url;")
	checkError(err)
	fmt.Println("Finished dropping table (if existed).")

	_, err = db.Exec("CREATE TABLE url (id serial PRIMARY KEY, long_url VARCHAR(255), short_url VARCHAR(255));")
	checkError(err)
	fmt.Println("Finished creating table.")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func addToBase(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)["urlKey"]
	long_url := "https://" + params + ".fr"
	sqlStatement, err := db.Prepare("INSERT INTO url (long_url, short_url) VALUES (?, ?);")
	res, err := sqlStatement.Exec(long_url, params)
	checkError(err)
	rowCount, err := res.RowsAffected()
	fmt.Printf("Inserted %d row(s) of data.\n", rowCount)
}

func getUrlById(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)["idUrl"]
	var (
		id        int
		long_url  string
		short_url string
	)
	rows, err := db.Query("SELECT id, long_url, short_url from url where id = ?", params)
	checkError(err)
	defer rows.Close()
	fmt.Println("Reading data:")
	for rows.Next() {
		err := rows.Scan(&id, &long_url, &short_url)
		checkError(err)
		fmt.Printf("Data row = (%d, %s, %d)\n", id, long_url, short_url)
	}
	err = rows.Err()
	checkError(err)
	fmt.Println("Redirecting to... ", long_url)
	http.Redirect(w, r, long_url, http.StatusFound)
}

func main() {
	connectDB()
	fmt.Println("----------------------------")
	fmt.Println("Server started on port 8080")
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/new/{urlKey}", addToBase).Methods("GET")
	router.HandleFunc("/get/{idUrl}", getUrlById).Methods("GET")
	log.Fatal(http.ListenAndServe(":8080", router))
}
